/* eslint-disable react/prop-types */

const NoteForm = (props) => {

  const {
    todoTitle, 
    setTodoTitle, 
    todoLists, 
    setTodoLists, 
    editMode, 
    editableNote, 
    setEditMode, 
    setEditableNote
  } = props;

  // Submit Handler
  const submitHandler = (event) => {
    event.preventDefault();
    if (todoTitle.trim() === "") return alert("Add Todo Item");

    editMode ? updateHandler() : createHandler();
  }

  // Create Handler
  const createHandler = () => {
    const newTodo = {
      id: Date.now() + "",
      title: todoTitle,
      status: "Pending",
      clicked: true
    }
    setTodoLists([...todoLists, newTodo]);
    setTodoTitle("");
    console.log("created");
  }


  // Update Handler
  const updateHandler = () => {
    const updateTodoList = todoLists.map((item) => {
      if (item.id === editableNote.id) {
        return { ...item, title: todoTitle }
      }
      return item;
    });
    setTodoLists(updateTodoList);
    setEditMode(false);
    setEditableNote(null)
    setTodoTitle("");
  }

  return (
    <form onSubmit={submitHandler} className="form">
      <input type="text" value={todoTitle} onChange={(event) => setTodoTitle(event.target.value)} />
      <button>{editMode ? "Update Item" : "Add Item"}</button>
    </form>
  )
}

export default NoteForm