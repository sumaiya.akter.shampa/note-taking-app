import { useState } from "react";
import './todo.css';
import NoteForm from "./NoteForm";
import NoteList from "./NoteList";

const NoteTodo = () => {
  const [todoTitle, setTodoTitle] = useState("");
  const [todoLists, setTodoLists] = useState([
    { id: "1156", title: "Task One", status: "completed" }
  ]);
  const [editMode, setEditMode] = useState(false);
  const [editableNote, setEditableNote] = useState(null);


  return (
    <div className="todo">
      <h1 className="text-white">Note Taking App</h1>

      <NoteForm
        todoTitle={todoTitle} 
        setTodoTitle={setTodoTitle} 
        todoLists={todoLists} 
        setTodoLists={setTodoLists} 
        editMode={editMode}
        editableNote={editableNote}
        setEditMode={setEditMode}
        setEditableNote={setEditableNote}
      />

      <NoteList
        setTodoTitle={setTodoTitle}
        todoLists={todoLists}
        setTodoLists={setTodoLists}
        setEditMode={setEditMode}
        setEditableNote={setEditableNote}
      />
    </div>
  )
}

export default NoteTodo;