/* eslint-disable react/prop-types */
import { Table, Button } from "react-bootstrap";

const NoteList = (props) => {
  const {
    setTodoTitle,
    todoLists,
    setTodoLists,
    setEditMode,
    setEditableNote,
  } = props;

  // Remove Handler
  const removeHandler = (todoListId) => {
    const updateTodos = todoLists.filter((item) => item.id !== todoListId);
    setTodoLists(updateTodos)
  }

  // Edit Handler
  const editHandler = (item) => {
    setEditMode(true);
    setTodoTitle(item.title);
    setEditableNote(item)

  }

  // Complete Handler
  const completeHandler = (todoListId) => {
    const updatedTodos = todoLists.map((item) => {
      if (item.id === todoListId) {
        return { ...item, status: item.status.toLowerCase() === "pending" ? "completed" : "pending", clicked: false };
      }
      return item;
    });
    setTodoLists(updatedTodos);
  }

  return (
    <div className="todo-list">
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Task</th>
            <th>Status</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {todoLists.map(item => (
            <tr key={item.id}>
              <td>{item.title}</td>
              <td>{item.status}</td>
              <td>
                <Button onClick={() => editHandler(item)}>Edit</Button>
                <Button onClick={() => removeHandler(item.id)}>Delete</Button>

                <Button
                  onClick={() => completeHandler(item.id)}
                  variant={item.status.toLowerCase() === "pending" ? "light" : "success"}>
                  {item.status.toLowerCase() === "pending" ? "Complete" : "Completed"}
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  )
}

export default NoteList